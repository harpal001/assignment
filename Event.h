#ifndef Event_h
#define Event_h

class Event
{
public:
    int total_capacity;
    int current_capacity;
    void addBooking();
    void cancelBooking();
    void ListAllEvents();
    void setAvailability(int availability);
    int getAvailability();
};

#endif