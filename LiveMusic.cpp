#include <iostream>
#include "Event.h"
#include "LiveMusic.h"
using namespace std;

LiveMusic::LiveMusic()
{
	total_capacity = 300;
	current_capacity = 300;
}

void LiveMusic::detailsAndAvailability()
{
	cout << "Welcome To Live music, the total capacity of this event is " << total_capacity << ", out of them " << total_capacity - current_capacity << " are already allocated, so now there is space for " << current_capacity << " people.\n\n";
}
