#include <iostream>
#include "Event.h"

using namespace std;

void Event::setAvailability(int availability)
{
	if (availability > total_capacity)
	{
		cout << "availability can't be larger than the total capacity\n";
	}
	else
	{
		current_capacity = availability;
	}
}

int Event::getAvailability()
{
	return current_capacity;
}

void Event::addBooking()
{
	if (current_capacity > 0)
	{
		current_capacity = current_capacity - 1;
		cout << "Booking is done\n";
	}
	else
	{
		cout << "Seats are not available\n";
	}
}

void Event::cancelBooking()
{
	if (total_capacity - current_capacity > 0)
	{
		current_capacity = current_capacity + 1;
		cout << "Booking is cancelled\n";
	}
}

void Event::ListAllEvents()
{
	cout << "There are 3 types of events\n";
	cout << "1) Live music\n";
	cout << "2) Standup comedy\n";
	cout << "3) Film\n";
}
