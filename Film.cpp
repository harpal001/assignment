#include <iostream>
#include <cstring>
#include "Event.h"
#include "Film.h"
using namespace std;

Film::Film(std::string type)
{
	total_capacity = 200;
	current_capacity = 200;
	typeOfFilm = type;
}

void Film::detailsAndAvailability()
{
	cout << "Welcome To Film, the total capacity of this event is " << total_capacity << ", out of them " << total_capacity - current_capacity << " are already allocated, so now there is space for " << current_capacity << " people. The type of this film is " << typeOfFilm << " .\n\n";
}
