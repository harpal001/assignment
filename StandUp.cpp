#include <iostream>
#include "Event.h"
#include "StandUp.h"
using namespace std;

StandUp::StandUp()
{
	total_capacity = 200;
	current_capacity = 200;
}

void StandUp::addBooking()
{
	if (current_capacity > 0)
	{

		seats[total_capacity - current_capacity] = 0;
		current_capacity = current_capacity - 1;
		cout << "Booking is done\n";
	}
	else
	{
		cout << "Seats are not available\n";
	}
}

void StandUp::cancelBooking()
{
	if (total_capacity - current_capacity > 0)
	{
		current_capacity = current_capacity + 1;
		seats[total_capacity - current_capacity] = 1;
		cout << "Booking is cancelled\n";
	}
}

void StandUp::detailsAndAvailability()
{
	cout << "Welcome To Stand up comedy, the total capacity of this event is " << total_capacity << ", out of them " << total_capacity - current_capacity << " are already allocated, so now there is space for " << current_capacity << " people.\n\n";
}
