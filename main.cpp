#include <iostream>
#include <fstream>
#include <string>
#include "Event.h"
#include "Film.h"
#include "StandUp.h"
#include "LiveMusic.h"
using namespace std;

int main()
{
    Event e;
    Film f2d("2d");
    Film f3d("3d");
    StandUp s;
    LiveMusic lm;
    int choice, option, c;

    while (1)
    {
        cout << "\n\nPlease choose from the following options\n";
        cout << "Press 1 for add a booking for an event\n";
        cout << "Press 2 for cancel/refund a booking\n";
        cout << "Press 3 for list all events\n";
        cout << "Press 4 for list details and availability of a given event\n";
        cout << "Press 5 for load data from a file (all events and availability)\n";
        cout << "Press 6 for save data to file (all events and availability)\n";
        cout << "Press any other key to quit\n";

        cin >> choice;

        if (choice == 1)
        {
            cout << "\n\nPlease choose from the following\n";
            cout << "Press 1 for Film booking\n";
            cout << "Press 2 for Live music booking\n";
            cout << "Press 3 for standup comedy booking\n";

            cin >> option;
            if (option == 1)
            {
                cout << "\n\n Please choose from the following\n";
                cout << "Press 1 for 2D film booking\n";
                cout << "Press 2 for 3D film booking\n";
                cin >> c;
                if (c == 1)
                {
                    f2d.addBooking();
                }
                else if (c == 2)
                {
                    f3d.addBooking();
                }
                else
                {
                    cout << "\nPlease choose valid option\n";
                }
            }
            else if (option == 2)
            {
                lm.addBooking();
            }
            else if (option == 3)
            {
                s.addBooking();
            }
            else
            {
                cout << "\nPlease enter valid option\n";
            }
        }
        else if (choice == 2)
        {

            cout << "\n\nPlease choose from the following\n";
            cout << "Press 1 for Film cancelling\n";
            cout << "Press 2 for Live music cancelling\n";
            cout << "Press 3 for standup comedy cancelling\n";

            cin >> option;
            if (option == 1)
            {

                cout << "\n\n Please choose from the following\n";
                cout << "Press 1 for 2D film cancelling\n";
                cout << "Press 2 for 3D film cancelling\n";
                cin >> c;
                if (c == 1)
                {
                    f2d.cancelBooking();
                }
                else if (c == 2)
                {
                    f3d.cancelBooking();
                }
                else
                {
                    cout << "\nPlease choose valid option\n";
                }
            }
            else if (option == 2)
            {
                lm.cancelBooking();
            }
            else if (option == 3)
            {
                s.cancelBooking();
            }
            else
            {
                cout << "\nPlease enter valid option\n";
            }
        }
        else if (choice == 3)
        {
            e.ListAllEvents();
        }
        else if (choice == 4)
        {
            cout << "\n\nPlease choose from the following\n";
            cout << "Press 1 to know details and availability about Film\n";
            cout << "Press 2 to know details and availability about Live music\n";
            cout << "Press 3 to know details and availability about standup comedy\n";

            cin >> option;
            if (option == 1)
            {
                cout << "\n\n Please choose from the following\n";
                cout << "Press 1 to know details and availability about 2D film\n";
                cout << "Press 2 to know details and availability about 3D film\n";
                cin >> c;
                if (c == 1)
                {
                    f2d.detailsAndAvailability();
                }
                else if (c == 2)
                {
                    f3d.detailsAndAvailability();
                }
                else
                {
                    cout << "\nPlease choose valid option\n";
                }
            }
            else if (option == 2)
            {
                lm.detailsAndAvailability();
            }
            else if (option == 3)
            {
                s.detailsAndAvailability();
            }
            else
            {
                cout << "\nPlease enter valid option\n";
            }
        }
        else if (choice == 5)
        {

            ifstream MyReadFile("events.txt");

            string myText;

            while (getline(MyReadFile, myText))
            {
                string eventName = "";
                string availability = "";
                int flag = 0;
                for (int i = 0; i < myText.length(); i++)
                {

                    if (myText[i] == '-')
                    {
                        flag = 1;
                    }
                    else if (flag == 0)
                    {

                        eventName.push_back(myText[i]);
                    }
                    else
                    {
                        availability.push_back(myText[i]);
                    }
                }

                int a = 0;
                a = stoi(availability);

                if (eventName == "Live music")
                {
                    lm.setAvailability(a);
                }
                else if (eventName == "Standup comedy")
                {
                    s.setAvailability(a);
                }
                else if (eventName == "Film2D")
                {
                    f2d.setAvailability(a);
                }
                else if (eventName == "Film3D")
                {
                    f3d.setAvailability(a);
                }
                else
                {
                    cout << "invalid entry\n";
                }
            }
        }
        else if (choice == 6)
        {

            ofstream myfile("events.txt");
            if (myfile.is_open())
            {
                myfile << "Live music-" << lm.getAvailability() << "\n";
                myfile << "Standup comedy-" << s.getAvailability() << "\n";
                myfile << "Film2D-" << f2d.getAvailability() << "\n";
                myfile << "Film3D-" << f3d.getAvailability() << "\n";
                myfile.close();
            }
            else
                cout << "Unable to open file";
        }
        else
        {
            break;
        }
    }

    return 0;
}