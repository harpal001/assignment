#ifndef Film_h
#define Film_h

class Film : public Event
{

public:
    std::string typeOfFilm;

    Film(std::string type);

    void detailsAndAvailability();
};

#endif