#ifndef Stand_Up_h
#define Stand_Up_h

class StandUp : public Event
{

public:
    int seats[200] = {1};

    StandUp();

    void addBooking();

    void cancelBooking();

    void detailsAndAvailability();
};

#endif